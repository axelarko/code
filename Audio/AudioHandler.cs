﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public sealed class AudioHandler
{
    public enum AudioType { NONE, MASTER, SFX, MUSIC, AMBIENT, VOICE};
    private delegate void AudioChange();


    private event AudioChange OnMasterVolumeChange = UpdateAllPlayers, OnMusicVolumeChange = UpdateMusicPlayer, OnSoundVolumeChange = UpdateSoundEffectPlayers;

    private static AudioHandler instance;
    private static AudioHandler Instance
    {
        get { return instance ?? (Instance = CreateInstance()); }
        set { instance = value; }
    }

    private static AudioHandler CreateInstance()
    {
        AudioHandler newInstance = new AudioHandler();
        return newInstance;
    }

    //-------------------- ALL ACTIVE PLAYERS --------------------

    private static AudioPlayer musicPlayer;
    private static AudioPlayer MusicPlayer
    {
        get { return musicPlayer ?? (MusicPlayer = CreateMusicPlayer()); }
        set { musicPlayer = value; }
    }

    private static List<AudioPlayer> soundEffectPlayers;
    private static List<AudioPlayer> SoundEffectPlayers
    {
        get { return soundEffectPlayers ?? (SoundEffectPlayers = new List<AudioPlayer>()); }
        set { soundEffectPlayers = value; }
    }

    private static AudioPlayer CreateSoundPlayer()
    {
        AudioPlayer player = AudioPlayer.CreateAudioPlayer();
        Object.DontDestroyOnLoad(player);
#if UNITY_EDITOR
        player.name = "Sound Player";
#endif
        return player;
    }

    private static AudioPlayer CreateMusicPlayer()
    {
        AudioPlayer player = AudioPlayer.CreateAudioPlayer();
        Object.DontDestroyOnLoad(player);
#if UNITY_EDITOR
        player.name = "Music Player";
#endif
        return player;
    }

    //-------------------- VOLUME FIELDS AND PROPERTIES --------------------
    
    private const float MAXVOLUME = 1, MINVOLUME = 0;
    private static float masterVolume = 0.5f, soundEffectVolume = 0.5f, musicVolume = 0.5f;

    public static float MasterVolume
    {
        get { return masterVolume; }
        private set { masterVolume = value > MAXVOLUME ? MAXVOLUME : value < MINVOLUME ? MINVOLUME : value; Instance.OnMasterVolumeChange?.Invoke(); }
    }
    public static float SoundEffectVolume
    {
        get { return soundEffectVolume; }
        private set { soundEffectVolume = value > MAXVOLUME ? MAXVOLUME : value < MINVOLUME ? MINVOLUME : value; Instance.OnSoundVolumeChange?.Invoke(); }
    }
    public static float MusicVolume
    {
        get { return musicVolume; }
        private set { musicVolume = value > MAXVOLUME ? MAXVOLUME : value < MINVOLUME ? MINVOLUME : value; Instance.OnMusicVolumeChange?.Invoke(); }
    }

    //-------------------- VOLUME PUBLIC METHODS --------------------

    public static float GetVolume(AudioType type)
    {
        switch (type)
        {
            case AudioType.MASTER:
                return MasterVolume;

            case AudioType.SFX:
                return SoundEffectVolume;

            case AudioType.MUSIC:
                return MusicVolume;
            default: return 0;
        }
    }

    public static float SetVolume(AudioType type, float value)
    {
        switch(type)
        {
            case AudioType.MASTER:
                return MasterVolume = value;

            case AudioType.SFX:
                return SoundEffectVolume = value;

            case AudioType.MUSIC:
                return MusicVolume = value;
            default: return 0;
        }
    }

    public static float SetMasterVolume(float value)
    {
        return MasterVolume = value;
    }

    public static float SetSoundEffectVolume(float value)
    {
        return SoundEffectVolume = value;
    }

    public static float SetMusicVolume(float value)
    {
        return MusicVolume = value;
    }
    
    public static AudioPlayer GetMusicPlayer()
    {
        return MusicPlayer;
    }

    public AudioHandler()
    {
       
    }

    static void UpdateAllPlayers()
    {
        foreach(AudioPlayer player in SoundEffectPlayers)
        {
            player.AudioSource.volume = MasterVolume * SoundEffectVolume;
        }
        MusicPlayer.AudioSource.volume = MasterVolume * MusicVolume;
    }

    static void UpdateMusicPlayer()
    {
        MusicPlayer.AudioSource.volume = MasterVolume * MusicVolume;
    }

    static void UpdateSoundEffectPlayers()
    {
        foreach (AudioPlayer player in SoundEffectPlayers)
        {
            player.AudioSource.volume = MasterVolume * SoundEffectVolume;
        }
    }

    public static void RemovePlayer(AudioPlayer player)
    {
        if(SoundEffectPlayers.Contains(player))
        {
            SoundEffectPlayers.Remove(player);
            return;
        }
    }
    
    public static AudioClip PlayMusic(string trackName = "")
    {
        //Use the dummied out ones later!
        //MusicPlayer.Play(Resources.Load<AudioClip>("Audio/Music/"+trackName), AudioType.MUSIC, true);
        return MusicPlayer.Play(AudioLibrary.GetMusic(trackName), AudioType.MUSIC, true);
    }

    public static void PlayMusic(AudioClip clip, float clipTime = 0)
    {
        if(clip == null || clip == MusicPlayer.AudioSource.clip)
        {
            return;
        }
        //Use the dummied out ones later!
        MusicPlayer.Play(clip, AudioType.MUSIC, true);
        //MusicPlayer.Play(AudioLibrary.GetMusic(trackName), AudioType.MUSIC, true);
        MusicPlayer.AudioSource.time = clipTime;
    }

    public static void StopMusic()
    {
        MusicPlayer.AudioSource.Pause();
    }

    public static void FadeToMusic(AudioClip clip = null)
    {
        if (clip == null)
        {
            return;
        }
        MusicPlayer.Play(clip, AudioType.MUSIC, true);
    }

    public static AudioClip PlaySoundEffect(string clipName)
    {
        //CreateSoundPlayer().Play(Resources.Load<AudioClip>("Audio/SFX/" + clipName), AudioType.SFX);
        return CreateSoundPlayer().Play(AudioLibrary.GetSoundEffect(clipName), AudioType.SFX);
    }

    public static AudioClip FadeInto(AudioClip clipToPlay, float fadeTime)
    {
        MusicPlayer.StartCoroutine(MusicFader(clipToPlay, fadeTime));
        return clipToPlay;
    }

    static IEnumerator MusicFader(AudioClip newMusic, float fadeTime, bool unscaledTime = true)
    {
        GameObject oldPlayer = MusicPlayer.gameObject;
        float currentFadeTime = 0;
        do
        {
            float timer = unscaledTime ? Time.unscaledDeltaTime : Time.deltaTime;
            currentFadeTime = currentFadeTime + timer > fadeTime ? fadeTime : currentFadeTime + timer;
            MusicPlayer.AudioSource.volume = MasterVolume * MusicVolume *  (1-currentFadeTime/fadeTime);
            yield return null;
        }
        while (currentFadeTime < fadeTime);
        AudioPlayer newPlayer = CreateMusicPlayer();
        newPlayer.Play(newMusic, AudioType.MUSIC, true);
        MusicPlayer = newPlayer;
        currentFadeTime = 0;
        do
        {
            float timer = unscaledTime ? Time.unscaledDeltaTime : Time.deltaTime;
            currentFadeTime = currentFadeTime + timer > fadeTime ? fadeTime : currentFadeTime + timer;
            MusicPlayer.AudioSource.volume = MasterVolume * MusicVolume * currentFadeTime/fadeTime;
            yield return null;
        }
        while (currentFadeTime < fadeTime);
        Object.Destroy(oldPlayer);
    }

    public static AudioClip Crossfade(AudioClip clipToPlay, float fadeTime)
    {
        AudioPlayer newMusicPlayer = CreateMusicPlayer();
        newMusicPlayer.AudioSource.clip = clipToPlay;
        newMusicPlayer.StartCoroutine(Crossfader(newMusicPlayer, fadeTime));
        return clipToPlay;
    }

    static IEnumerator Crossfader(AudioPlayer newPlayer, float fadeTime)
    {
        newPlayer.AudioSource.Play();
        float currentFadeTime = 0;
        do
        {
            MusicPlayer.AudioSource.volume = MasterVolume * MusicVolume * (fadeTime - currentFadeTime);
            newPlayer.AudioSource.volume = MasterVolume * MusicVolume * currentFadeTime;
            currentFadeTime = currentFadeTime + Time.unscaledDeltaTime > fadeTime ? fadeTime : currentFadeTime + Time.unscaledDeltaTime;
            yield return null;
        }
        while (currentFadeTime < fadeTime);
        Object.Destroy(MusicPlayer.gameObject);
        MusicPlayer = newPlayer;
    }
}


public class AudioPlayer : MonoBehaviour
{
    private AudioClip Clip
    {
        get; set;
    }

    private AudioClip MissingClip()
    {
        if(AudioSource.clip != null)
        {
            return AudioSource.clip;
        }
#if UNITY_EDITOR
        print("ERROR - AUDIOCLIP MISSING FROM AUDIOPLAYER");
#endif
        return null;
    }

    private AudioSource audioSource;
    public AudioSource AudioSource
    {
        get { return audioSource ?? (AudioSource = gameObject.AddComponent<AudioSource>()); }
        private set { audioSource = value; }
    }

    public bool IsPlaying
    {
        get { return AudioSource.isPlaying; }
    }

    public AudioClip Play(AudioClip clipToPlay,  AudioHandler.AudioType type = AudioHandler.AudioType.SFX, bool looping = false)
    {
        if (AudioHandler.MasterVolume == 0 && type != AudioHandler.AudioType.MUSIC)
            {
#if UNITY_EDITOR
            Debug.Log("Master volume is zero");
#endif
            return null;
            }

        Clip = clipToPlay;

        float actualVolume = 0;
        switch (type)
        {
            case AudioHandler.AudioType.SFX:
                actualVolume = AudioHandler.SoundEffectVolume;
                break;
            case AudioHandler.AudioType.MUSIC:
                actualVolume = AudioHandler.MusicVolume;
                break;

        }
        AudioSource.clip = clipToPlay;
        AudioSource.loop = looping;
        AudioSource.volume = AudioHandler.MasterVolume * actualVolume;
        AudioSource.Play();
                
        return clipToPlay;
    }

    public static AudioPlayer CreateAudioPlayer()
    {
        AudioPlayer player = new GameObject().AddComponent<AudioPlayer>();
        player.AudioSource = player.gameObject.AddComponent<AudioSource>();
        return player;
    }


    void Terminate()
    {
        AudioHandler.RemovePlayer(this);
        gameObject.SetActive(false);
        Destroy(gameObject);
    }

    void Update()
    {
        if (!IsPlaying && AudioHandler.GetMusicPlayer() != this)
        {
            if (AudioSource.pitch < 0)
            {
                AudioSource.Play();
                AudioSource.time = Clip.length - 0.1f;
                return;
            }
            Terminate();
        }
    }
}
