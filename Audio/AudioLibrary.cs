﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class AudioLibrary : ScriptableObject
{
#if UNITY_EDITOR
    [SerializeField]
#endif
    private AudioClip[] soundEffects = new AudioClip[0];

#if UNITY_EDITOR
    [SerializeField]
#endif
    private AudioClip[] musicTracks = new AudioClip[0];

    private static AudioLibrary instance;
    public static AudioLibrary Instance
    {
        get
        {
            if(instance == null)
            {
                SetupLibrary();
            }
            return instance;
        }
        private set { instance = value; }
    }


    private Dictionary<string, AudioClip> _soundEffects;
    private Dictionary<string, AudioClip> SoundEffects
    {
        get { return _soundEffects == null ? SoundEffects = SetupSoundEffects() : _soundEffects; }
        set { _soundEffects = value; }
    }

    private Dictionary<string, AudioClip> _musicTracks;
    private Dictionary<string, AudioClip> MusicTracks
    {
        get { return _musicTracks == null ? MusicTracks = SetupMusicTracks() : _musicTracks; }
        set { _musicTracks = value; }
    }

    private static Dictionary<string, AudioClip> SetupSoundEffects()
    {
        Dictionary<string, AudioClip> sEffects = new Dictionary<string, AudioClip>();
        foreach(AudioClip clip in Instance.soundEffects)
        {
            if(!sEffects.ContainsValue(clip))
            {
                sEffects.Add(clip.name, clip);
            }
        }
        return sEffects;
    }

    public static AudioClip GetSoundEffect(string name)
    {
        AudioClip value;
        if(Instance.SoundEffects.TryGetValue(name, out value))
        {
            return value;
        }
        return null;
    }

    public static AudioClip GetMusic(string name)
    {
        AudioClip value;
        if (Instance.MusicTracks.TryGetValue(name, out value))
        {
            return value;
        }
        return null;
    }

    private static Dictionary<string, AudioClip> SetupMusicTracks()
    {
        Dictionary<string, AudioClip> mTracks = new Dictionary<string, AudioClip>();
        foreach (AudioClip clip in Instance.musicTracks)
        {
            if (!mTracks.ContainsValue(clip))
            {
                mTracks.Add(clip.name, clip);
            }
        }
        return mTracks;
    }

    [RuntimeInitializeOnLoadMethod()]
    private static void SetupLibrary()
    {
        Loader loader = new GameObject().AddComponent<Loader>();
        loader.StartCoroutine(LoadLibrary(loader));
    }

    IEnumerator LoadAllAudioFiles()
    {
        AudioClip[] sfx = Resources.LoadAll<AudioClip>("Audio/SFX");
        soundEffects = sfx;
        AudioClip[] music = Resources.LoadAll<AudioClip>("Audio/Music");
        musicTracks = music;
        yield return null;
    }

    static IEnumerator LoadLibrary(Loader loader)
    {
        ResourceRequest loading = Resources.LoadAsync<AudioLibrary>("Audio/AudioLibrary");
        DontDestroyOnLoad(loader);
        AudioLibrary lib = null;
        if (loading.asset == null)
        {
#if UNITY_EDITOR
            if(!System.IO.Directory.Exists("Assets/Resources/Audio"))
            {
                System.IO.Directory.CreateDirectory("Assets/Resources/Audio");
            }
            lib = CreateInstance<AudioLibrary>();
            Debug.Log("Error! Cannot find Audio Library! Creating new instance!");
            UnityEditor.AssetDatabase.CreateAsset(lib, "Assets/Resources/Audio/AudioLibrary.asset");
            UnityEditor.AssetDatabase.SaveAssets();
            UnityEditor.AssetDatabase.Refresh();
#endif
        }
        else while(!loading.isDone)
        {
            yield return null;
        }
        Instance = loading.asset == null ? lib : (AudioLibrary)loading.asset;
#if UNITY_EDITOR
        if(Instance != lib)
        {
            yield return loader.StartCoroutine(Instance.LoadAllAudioFiles());
        }
        Debug.Log("Audio Library Successfully loaded");
#else
        yield return loader.StartCoroutine(Instance.LoadAllAudioFiles());
#endif
        Destroy(loader.gameObject);
    }
}

class Loader : MonoBehaviour
{
    
}