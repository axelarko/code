﻿using System;

/// <summary>
/// Health class for characters.
/// </summary>
[Serializable]
public sealed class Health
{
    [NonSerialized]
    private readonly const int MAX = 9999, MIN = 0;

    private int current = 0, max = 0;
    [field: NonSerialized]
    //Event raised each time Current or Max health changes
    public event DelegateMethod OnHealthChanged;
    //Property for Current Health. Makes sure Current is never out of bounds and raises OnHealthChanged whenever value changes.
    public int Current
    {
        get
        {
            return current < MIN ? MIN : current > Max ? Max : current;
        }

        private set
        {      
            current = value < MIN ? MIN : value > Max ? Max : value;
            OnHealthChanged?.Invoke();          
        }
    }
    //Property for Max Health. Work similarly to Current.
    public int Max
    {
        get { return max < 0 ? MIN + 1 : max > MAX ? MAX : max; }
        private set
        {
            max = value < MIN + 1 ? MIN + 1 : value > MAX ? MAX : value;
            OnHealthChanged?.Invoke();
        }
    }
    //Returns the current health percentage
    public float Percentage
    {
        get { return (float)(decimal)Current / Max * 100; }
    }
    #region Constructors
    public Health()
    {
        Current = Max;
    }
    public Health(int value) : base()
    {
        Max = value;      
    }
    #endregion
    /// <summary>
    /// Sets Current Health to value
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public int Set(int value)
    {
        return Current = value;
    }
    /// <summary>
    /// Sets Max Health to value
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public int SetMax(int value)
    {
        return Max = value;
    }
    /// <summary>
    /// Adds a value to Current Health
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public int Change(int value)
    {
        return Current += value;
    }    
    #region HealthStrings
    //Methods used for returning information regarding health status. Primarily for battle logs
    
    /// <summary>
    /// string of current HP
    /// </summary>
    /// <returns></returns>
    public string StringCurrent()
    {
        return $"{Current}";
    }

    /// <summary>
    /// string of maximum HP
    /// </summary>
    /// <returns></returns>
    public string StringMax()
    {
        return $"{Max}";
    }

    /// <summary>
    /// string of current/maximum HP
    /// </summary>
    /// <returns></returns>
    public string StringTotal()
    {
        return $"{Current}/{Max}";
    }

    /// <summary>
    /// string of health %
    /// </summary>
    /// <returns></returns>
    public string StringPercentage()
    {
        return $"{Percentage:0.##}%";
    }
    #endregion
}
