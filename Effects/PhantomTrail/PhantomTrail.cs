﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[RequireComponent(typeof(SpriteRenderer))]
public class PhantomTrail : MonoBehaviour
{
    [field: SerializeField]
    protected virtual SpriteRenderer MainRenderer { get; set; }
    private float SpawnRate { get; } = 0.1f;
    private float TrailLifetime { get; } = 1f;
    private float CurrentTime { get; set; } = 0;
    private int MaxPhantoms { get; set; } = 0;
    [field: SerializeField]
    private bool OverrideColor { get; set; } = false;
    [field: SerializeField]
    private Color Color = Color.white;
    private AnimationCurve SizeDelta { get; set; } = new AnimationCurve(new Keyframe(0, 1), new Keyframe(1, 1));

    private Phantom[] Phantoms { get; set; } = new Phantom[1];
    private GameObject PhantomParent { get; set; }

    private void Awake()
    {
        MainRenderer = MainRenderer == null ? GetComponent<SpriteRenderer>() : MainRenderer;
        SetupPhantoms();
    }

    private void Update()
    {
        Timestep();
    }

    public void Activate()
    {
        this.enabled = true;
    }

    public void Deactivate()
    {
        this.enabled = false;
    }

    void Timestep()
    {
        CurrentTime += Time.deltaTime;
        if (CurrentTime >= SpawnRate)
        {
            CurrentTime -= SpawnRate;
            SpawnPhantom();
        }
    }

    void SetupPhantoms()
    {
        if(PhantomParent != null)
        {
            Destroy(PhantomParent.gameObject);
        }
        MaxPhantoms = Mathf.CeilToInt(1 / (SpawnRate <= 0 ? Mathf.Epsilon : SpawnRate) * TrailLifetime) + 5;
        if (MaxPhantoms > 1 / Time.unscaledDeltaTime)
        {
            MaxPhantoms = Mathf.CeilToInt(1 / Time.unscaledDeltaTime) + 5;
        }
        PhantomParent = new GameObject($"{gameObject.name}'s Trail ({MaxPhantoms})");     
        Phantoms = new Phantom[MaxPhantoms];
        for (int i = 0; i < Phantoms.Length; i++)
        {
            Phantom phantom = Phantoms[i] = new GameObject().AddComponent<Phantom>();
            phantom.transform.parent = PhantomParent.transform;
            phantom.SpriteRenderer.sortingOrder = MainRenderer.sortingOrder;
            phantom.gameObject.name = $"Phantom {i + 1}";
            phantom.gameObject.SetActive(false);
        }
    }

    void SpawnPhantom()
    {
        foreach(Phantom phantom in Phantoms)
        {
            if(!phantom.isActiveAndEnabled)
            {
                phantom.gameObject.SetActive(true);
                phantom.Spawn(MainRenderer, OverrideColor ? Color: Color.white, SizeDelta, transform, TrailLifetime);
                return;
            }
        }
#if UNITY_EDITOR
        Debug.LogWarning("We're out of ghosts!");
#endif
    }

    [RequireComponent(typeof(SpriteRenderer))]
    class Phantom : MonoBehaviour
    {
        public SpriteRenderer SpriteRenderer { get; private set; }
        private float LifeTime { get; set; }
        private float CurrentLifetime { get; set; }
        private AnimationCurve SizeDelta { get; set; }

        void Awake()
        {
            SpriteRenderer = GetComponent<SpriteRenderer>();
        }

        public void Spawn(SpriteRenderer mainRenderer, Color color, AnimationCurve sizeDelta, Transform transform, float lifetime)
        {
            SpriteRenderer.sprite = mainRenderer.sprite;
            SpriteRenderer.color = color;
            SpriteRenderer.flipX = mainRenderer.flipX;
            SpriteRenderer.flipY = mainRenderer.flipY;
            SizeDelta = sizeDelta;
            CurrentLifetime = LifeTime = lifetime <= 0 ? Mathf.Epsilon : lifetime;
            this.transform.position = transform.position;
            this.transform.rotation = transform.rotation;
        }

        private void Update()
        {
            CurrentLifetime -= Time.deltaTime;
            if (CurrentLifetime <= 0)
            {
                gameObject.SetActive(false);
                return;
            }
        }

        private void LateUpdate()
        {        
            float alpha = (CurrentLifetime / LifeTime) * 0.5f;
            SpriteRenderer.sortingOrder = -100 + Mathf.RoundToInt(alpha * 200);
            SpriteRenderer.color = new Color(SpriteRenderer.color.r, SpriteRenderer.color.g, SpriteRenderer.color.b, alpha);
            transform.localScale = new Vector2(1, 1) * (SizeDelta.Evaluate(1 - CurrentLifetime / LifeTime));
        }
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(PhantomTrail))]
    class PhantomTrailEditor : Editor
    {
        [field: SerializeField]
        private bool SizeOverLifetime { get; set; }

        public override void OnInspectorGUI()
        {
            PhantomTrail trail = target as PhantomTrail;
            trail.MainRenderer = EditorGUILayout.ObjectField("Sprite Renderer", trail.MainRenderer, typeof(SpriteRenderer), true) as SpriteRenderer;
            if (trail.OverrideColor = EditorGUILayout.Toggle("Override Color?", trail.OverrideColor))
            {
                trail.Color = EditorGUILayout.ColorField("Color", trail.Color);
            }
            if(SizeOverLifetime = EditorGUILayout.Toggle("Size over lifetime?", SizeOverLifetime))
            {
                trail.SizeDelta = EditorGUILayout.CurveField(label: "SizeDelta", trail.SizeDelta) as AnimationCurve;
            }
            else
            {
                trail.SizeDelta = new AnimationCurve(new Keyframe(0, 1), new Keyframe(1, 1));
            }
        }
    }
#endif
}