﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//If you've ever changed a scene, note that all objects from the previous are typically destroyed in the process. This can be worked around in several ways. 
public class Persistence : MonoBehaviour
{
    //This example uses a Singleton reference.
    public static Persistence instance;

	// Use this for initialization
	void Awake ()
    {
        //A problem that might occur during scene change (especially when going back to previous scenes) is that the object which should be persistent has travelled to a point where it already exists.
        //This can cause various problems as well as ruining the Singleton reference by assigning it to a new instance while the old one is kept unreachable forever (this is called a Memory Leak - unused data being left
        //at an inaccessible point, eventually flooding the RAM if it gets out of hand).
        //To prevent this, we can simply let the object check if the singleton is assigned. If it is, the object will terminate. Otherwise, things are good to go. This means the first instance will set itself and 
        //be persistent throughout the program, but any duplicate objects will instantly destroy themselves.
        if(instance != null)
        {
            Destroy(this.gameObject);
            //Return makes sure the code does not go any further by ending the Method.
            return;
        }
        instance = this;

        //By using DontDestroyOnLoad, we tell unity to keep the object even during scene change
        DontDestroyOnLoad(this);
	}

}
