﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//This is an example class using the enum type to create a statemachine operating through Update. A statemachine is a way of letting the code reach specific points depending on what state the object is in. This can be applied to
// for example sequential work, AI logic and protocols. Note that this state machine example runs almost exactly like the coroutine example. They are completely interchangeable in what they do, they just do the same
//differently.
public class StateMachines : MonoBehaviour
{
    private int timesLooped = 0;
    //This is the enum collection we'll be using.
    private enum State { None, Moving, Waiting };
    //This variable holds our object's current state.
    private State currentState = State.None;

    private Vector3 randomPoint;
    private float waitTime;


    private void Start()
    {
        ChangeState(State.Moving);
    }

    //We don't want to clutter our Update Method with lots of logic, so instead we call the StateCheck from update.
    private void Update()
    {
        StateCheck();
    }

    void StateCheck()
    {
        //A switch block behaves similarly to an if-else block. It operates on one variable and can only end in one outcome. Switches are slightly faster than if-else blocks and can at times also be more readable.
        switch(currentState)
        {
            case State.Moving:
                MoveToRandomPoint();
                break;
            case State.Waiting:
                Wait();
                break;
            case State.None:
            default:
                //Note that multiple cases can land on the same result. An individual case may not occur more than once in a switch block however. default is a special case, it acts similar to an 'else' with no additional
                //boolean checks.
                break;
        }
    }

    //While we can change the state variable directly, we can do it through a method in order to do any additional required work.
    void ChangeState(State nextState)
    {
        switch(nextState)
        {
            //We need to assign our random point only once and thus we do that in the beginning of the Moving State
            case State.Moving:
                randomPoint = new Vector3(Random.Range(-10, 10), Random.Range(-10, 10));
                break;
                //Likewise, we need to reset our waittime once the Waiting State is set
            case State.Waiting:
                waitTime = 1f;
                break;
        }
        //We don't need to assign the current state in the switch block, since no matter the outcome, we set the currentState to the nextState. It is always good to avoid repetitions in code.
        currentState = nextState;
    }

    //This method moves our object to our assigned random point
    private void MoveToRandomPoint()
    {
        if (timesLooped >= 5)
        {
            //If we've looped enough times, we set the state to None, which will do nothing, as we have no logic moving us from that state outside of start.
            Debug.Log("Looped enough times. Terminating statemachine.");
            currentState = State.None;
            return;
        }
        //Comparing exact positions using floating numbers does not always return an exact match, even though they technically are the same (Difference may be one millionth of one, but that is still a difference)
        //so instead, we use Vector3.Distance and Mathf.Epsilon(Epsilon is a very, very tiny number typically used to bridge imprecisions such as this one) which will accurately tell if we are ALMOST EXACTLY at our
        //target point.
        transform.position = Vector3.MoveTowards(transform.position, randomPoint, Time.deltaTime * 10);
        if (Vector3.Distance(transform.position, randomPoint) <= Mathf.Epsilon)
        {
            //If we're within reasonable range, we change to the next state.
            ChangeState(State.Waiting);
        }
    }
    
    //This method subtracts from WaitTime and once it's at or below 0, we change state and add one to the loop counter.
    private void Wait()
    {
        waitTime -= Time.deltaTime;
        if (waitTime <= 0)
        {
            timesLooped++;
            ChangeState(State.Moving);
        }
    }
}

