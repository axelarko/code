﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Coroutines are useful ways of letting work be divided into a certain timeframe without requiring the use of too many variable dependencies. If used well, coroutines can make code easier to read while dividing it into
//efficient tasks. Note that this example does the exact same work as the state machine example, but does it through coroutines. 
public class Coroutines : MonoBehaviour
{

    int timesLooped = 0;

    private void Start()
    {
        StartCoroutine(MoveToRandomPoint());
    }


    //Coroutines use an IEnumerator's yield in order to suspend their operations temporarily. If one tried using a while-loop in a regular method, one would either cause the game to hitch or freeze completely.
    //An IEnumerator must always have at least one yield and they should be placed within loops or other iterative logic calls. The 'yield return null' is the most commonly used one, which returns nothing.
    private IEnumerator MoveToRandomPoint()
    {
        if(timesLooped >= 5)
        {
            //A yield break terminates the Coroutine at the point where it is called. It works very similarly to a 'return;' in a void-type Method. In this example we terminate the coroutine if we've done this loop
            //more than 4 times.
            Debug.Log("Looped enough times. Terminating coroutine.");
            yield break;
        }
        //In this example, we assign a random point once the coroutine starts.
        Vector3 randomPoint = new Vector3(Random.Range(-10, 10), Random.Range(-10, 10));
        //We can use a while conditional to make sure the coroutine keeps running until the while conditional is evaluated as false. In this particular case, we do not compare exact positions. This is because
        //comparing exact positions using floating numbers does not always return an exact match, even though they technically are the same (Difference may be one millionth of one, but that is still a difference)
        //so instead, we use Vector3.Distance and Mathf.Epsilon(Epsilon is a very, very tiny number typically used to bridge imprecisions such as this one) which will accurately tell if we are ALMOST EXACTLY at our
        //target point.
        while(Vector3.Distance(transform.position, randomPoint) > Mathf.Epsilon)
        {
            //Vector3.MoveTowards is a handy method for homing objects.
            transform.position = Vector3.MoveTowards(transform.position, randomPoint, Time.deltaTime * 10);
            yield return null;
        }
        //Coroutines can call other coroutines and they can run simultaneously as well. In this example however, we run them in sequence to emulate a state machine.
        StartCoroutine(Wait(1));
    }

    //IEnumerators can use parameters just like conventional Methods
    private IEnumerator Wait(float waitTime)
    {
        //Instead of using a yield return new WaitforSeconds we can simply tick down our timer. The advantage of this is that we can use the logic within the loop to for example do things incrementally or sequentially.
        //(e.g. fading or moving things over a period of time)

        //Coroutines also have the advantage of being able to use local variables (waitTime in this example) in a much more persistent way than conventional methods. In coroutines, they behave similar to field
        //variables as they carry over from frame to frame.
        while(waitTime > 0)
        {
            waitTime -= Time.deltaTime;
            yield return null;
        }
        timesLooped++;
        StartCoroutine(MoveToRandomPoint());
    }
}
