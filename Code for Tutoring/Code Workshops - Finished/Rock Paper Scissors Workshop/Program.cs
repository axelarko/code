﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class Program
{
    //This program is a game which plays Rock, Paper, Scissors with against the computer. Here we practice logical comparisons, an alternative way of handling input in addition to using values stored in arrays throughout the program

    static void Main(string[] args)
    {
        //This array of strings is the variable holding our three choices in the game.
        string[] choices = new string[] { "Rock", "Paper", "Scissors" };

        //These variables hold the player's and computer's scores. Note the comma after playerScore. A comma in this situation means we can type additional variables OF THE SAME TYPE as the first without having to redeclare the type.
        //This is equavilent of typing: int playerScore = 0; int computerScore = 0;
        int playerScore = 0, computerScore = 0;
        
        //Like previous workshops, we use a boolean value for our game loop. As long as gameIsRunning is true, the loop will keep repeating once it reaches its endpoint.
        bool gameIsRunning = true;
        while (gameIsRunning)
        {
            //To give our player a heads-up of what's going on, we write an introduction string on the screen. By using the {} placeholders in a formatted string, we can access the choices array and use the values there instead of 'hard coding' the text.
            string greeting = string.Format("Welcome to {0}, {1}, {2}!", choices[0], choices[1], choices[2]);
            Console.WriteLine(greeting);

            //At the beginning of each game loop, we want to reset our scores. Note that = is used twice. In this example, we have the value 0 which we assign to computerScore. We then assign the same value to playerScore. This is equavilent of typing
            //playerScore = 0; computerScore = 0; or computerScore = 0; playerScore = computerScore;
            playerScore = computerScore = 0;

            //This is our inner game loop. This will keep running the code within until either the player's or the computer's score is equal to 5 (or more than 4 to be precise)
            while (playerScore < 5 && computerScore < 5)
            {

                string informationString = string.Format("The current score is {0} to {1}", playerScore, computerScore);
                Console.WriteLine(informationString);

                //We will be using playerString to hold a string from choices later on. Since variables need to be assigned before being used within methods, we do so here.
                string playerString = "";
                //To make sure we do not proceed without receiving valid input from our player, we create a while-loop here.
                bool invalidKeyPress = true;
                while (invalidKeyPress)
                {
                    //At the start of the loop we set invalidKeyPress to false. We do this because there is only one scenario where we need to set it back to true, which is if the player does not input a valid button.
                    invalidKeyPress = false;
                    informationString = string.Format("Press 1 for {0}, press 2 for {1} or press 3 for {2}!", choices[0], choices[1], choices[2]);
                    Console.WriteLine(informationString);

                    //A ConsoleKey is a type called enum, which behaves similarly to ints, but is also a named constant. Enums provide good readability overall, which is possibly one of the reasons why each keyboard button has one enum value assigned to them.
                    ConsoleKey playerInput = Console.ReadKey(true).Key;
                    //This is a switch block. It operates similarly to a line of if-else statements. A swtich block may only run one of the cases however, though multiple cases can lead to the same result.
                    switch (playerInput)
                    {
                        case ConsoleKey.D1:
                            playerString = choices[0];
                            break;
                        case ConsoleKey.D2:
                            playerString = choices[1];
                            break;
                        case ConsoleKey.D3:
                            playerString = choices[2];
                            break;
                        default:
                            playerString = "an invalid button";
                            invalidKeyPress = true;
                            break;
                    }
                    Console.WriteLine("You pressed {0}", playerString);
                }

                //The computer does not need to handle any input. Instead, it randomizes one value from choices which is then assigned to the computerString variable.
                int randomNumber = new Random().Next(0, choices.Length);
                string computerString = choices[randomNumber];

                Console.WriteLine("Computer chose {0}", computerString);

                //All we need to do now is cover all possible outcomes of Rock, Paper Scissors. First is the draw condition - no matter what the player has chosen, we know that if that is the same as the computer's choice, it will be a draw. By doing this, we
                //don't need to compare three different draw scenarios. Yay, logic!
                if (playerString == computerString)
                {
                    Console.WriteLine("The round is a draw");
                }
                //In case there was not a draw, we need to find all cases where the computer wins. This looked slightly different during the workshop, but the functionality is exactly the same.
                else if (playerString == choices[0] && computerString == choices[1])
                {
                    computerScore++;
                    Console.WriteLine("Computer wins");
                }
                else if
                (playerString == choices[1] && computerString == choices[2])
                {
                    computerScore++;
                    Console.WriteLine("Computer wins");
                }
                else if
                (playerString == choices[2] && computerString == choices[0])
                {
                    computerScore++;
                    Console.WriteLine("Computer wins");
                }
                //Since we know that unless there is a draw or the computer wins, the player logically has to win as there are no other possible outcomes. Thus we can just do an else-block for player scoring.
                else
                {
                    playerScore++;
                    Console.WriteLine("Player wins");
                }
                //And this is the end point of the game loop. It will repeat if its condition is still true (if either player has less than 5 points)
            }

            
            //Depending on which has the higher score, we assign either "Player" or "Computer" to the victor string.
            string victor = "";
            if(playerScore > computerScore)
            {
                victor = "Player";
            }
            else
            {
                victor = "Computer";
            }
            //Before ending the game, we print the victor on screen and give the player a prompt if they wish to play again.
            string endOfGameString = string.Format("{0} wins! Do you wish to play again? Y/N", victor);
            Console.WriteLine(endOfGameString);

            //Since we require either Y or N to be pressed, we keep the code looping until either condition is fullfilled
            bool awaitingPlayerInput = true;
            while(awaitingPlayerInput)
            {
                ConsoleKey inputKey = Console.ReadKey(true).Key;
                if (inputKey == ConsoleKey.Y)
                {
                    //Should the player press Y, we make the loop exit which means we reach the end of the gameIsRunning loop. This means we will restart the game at the start of the loop. Console.Clear removes all text from the console window.
                    //Cleaning up old text before starting afresh is always nice.
                    awaitingPlayerInput = false;
                    Console.Clear();
                }
                else if (inputKey == ConsoleKey.N)
                {
                    //Setting awaitingPlayerInput to false here would have the same results as using return, except for the fact that the gameIsRunning bool is still set to true. Turning that to false as well would have the same effect as using return.
                    //In this example it is a matter of preference, though return is less text to type.
                    return;
                }
            }           
        }
    }
}





















/*TODO
         * Practice more if-else
         * Expand logics to switches with case: break; branches
         * Let class work on comparisons for using boolean values
         * Keep it simple
         * Keep practicing syntax, assigning values to variables, the implications of what variables represent and general guidelines
         * 
         * 
         * */

/*string itemOneName = "Apples", itemTwoName = "Bananas";

    string[] itemNames = new string[]
        {
            "Apples",
            "Bananas",
            "Lemons",
            "Pears",
            "Pineapples",
            "Figs",
            "Tomatoes",
            "Melons",
            "Honeymelons",
            "Watermelons",
            "Garbage Bins"
        };

    for(int i = 0; i < itemNames.Length; i++)
    {
        string fruit = itemNames[i];
        Console.WriteLine(fruit);
    }

    foreach(string fruitName in itemNames)
    {
        Console.WriteLine(fruitName);
    }

    Console.ReadKey();*/
