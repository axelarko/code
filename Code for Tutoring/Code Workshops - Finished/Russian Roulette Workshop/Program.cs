﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//For this workshop we are creating a program which plays Russian Roulette with the player (And without a real gun, thank goodness). This workshop practices assigning values to arrays, simple methods and game flow.
class Program
{
    //First and foremost we declare our 
    //Here we define the buttonpresses required for handling our game. The ConsoleKey enum can be accessed when using the Console.ReadKey().Key property and each button has one distinct ConsoleKey assigned to them. Pretty handy.
    static ConsoleKey shootButton = ConsoleKey.Spacebar, spinButton = ConsoleKey.R;
    //Our six-shooter needs to chamber six bullets. Since we still work with mostly value-type data, let's use an array of strings to store them.
    static string[] chamber = new string[6];
    //We will only be playing against ourselves for this workshop and track our survived turns with this int variable.
    static int playerScore = 0;
    //This boolean will be what keeps our code in the main game loop.
    static bool gameIsRunning = false;

    static void Main(string[] args)
    {
        //Right off the bat, we call our StartGame method. Using methods to split the code in different functional branches will provide a much clearer overview and reusable code.
        StartGame();
    }

    static void StartGame()
    {
        //StartGame initializes playerScore in addition to printing a greeting string on screen, assigning a freshly randomized string array to our chamber before initiating the game loop.
        playerScore = 0;
        string output = string.Format("Welcome to Russian Roulette!");
        chamber = RandomizeBullets();
        Console.WriteLine(output);
        RunGameLoop();
    }

    //This is our method holding the game loop. It operates identically to having our while-loop in the main method, though being separated means we can call it manually whenever we wish to restart it.
    static void RunGameLoop()
    {
        gameIsRunning = true;
        while (gameIsRunning)
        {
            //The game loop will keep calling the PlayerTurn method each iteration it runs.
            PlayerTurn();
        }
        //We end up here once the game loop has ended. This method call simply restarts the game once that happens.
        StartGame();
    }

    //
    static void PlayerTurn()
    {
        //This is a special 'conditional operator'. It is identical in functionality as writing  'string turnNumber = ""; if (playerScore == 1) turnNumber = "turn"; else turnNumber = "turns";' It is mostly here for demonstration of different operators and
        //is also a very compact way of assigning values based on simple conditions! Using it is a matter of preference though.
        string turnNumber = playerScore == 1 ? "turn" : "turns";
        //Since we assign our turnNumber string above, we can use it in a formatted string for correct grammar before printing it on screen.
        string scoreString = string.Format("You have survived for {0} {1}", playerScore, turnNumber);
        Console.WriteLine(scoreString);

        //This is the complicated part of our PlayerTurn method. The rules for our game is that we must fire once each turn, but have the option to spin the chamber and randomize it once every turn, no more, no less. For readability's sake, we use two bools
        //to hold these conditions for us and we wish to remain in this part until we actually pull the trigger.
        bool playerHasNotShot = true;
        bool playerHasSpun = false;
        while (playerHasNotShot)
        {
            //First and foremost, we need to receive input. We then check whether or not the button corresponts to our defined input variables (shootButton & spinButton)
            ConsoleKey input = Console.ReadKey(true).Key;
            //If it is the shootButton, we call the Shoot method and assign its value to playerHasNotShot, since Shoot returns a boolean value.
            if (input == shootButton)
            {
                playerHasNotShot = Shoot();
            }
            //We only call the Spin method if we press the spinButton and we have not already randomized this turn.
            else if (input == spinButton && playerHasSpun == false)
            {
                playerHasSpun = Spin();
            }
        }
    }

    static bool Spin()
    {
        //First we assign a freshly randomized string array to our chamber before printing some info on screen and then returning the a bool set to true. (Value types do not need to be returned as variables. We can simply return them directly as values.
        //Anything that goes in a regular value assignment (e.g. int number = 5;) can be interpreted as something returned as an explicit value. It follows the same behaviour pretty much.
        chamber = RandomizeBullets();
        string spinString = "You spin the chambers";
        Console.WriteLine(spinString);
        return true;
    }

    static bool Shoot()
    {
        //The shoot method first checks the first string in our chamber array. If it is a bullet, we inform the player that they just made a terrible decision before setting the gameIsRunning bool to false to exit the game loop once the turn is completed.
        if (chamber[0] == "Bullet")
        {
            string turnNumber = playerScore == 1 ? "turn" : "turns";
            string lossString = string.Format("BANG! You have died. You survived for {0} {1}", playerScore, turnNumber);
            Console.WriteLine(lossString);
            gameIsRunning = false;
            //Keep in mind that returning from a method means the method will not run any code below where return is called. It's handy for flow control.
            return false;
        }
        //Should the first chamber not contain a bullet this is where we will end up. We add one to the playerScore right away.
        playerScore++;
        Console.WriteLine("*Click*");
        //This loop overwrites each string in the chamber array with the value of the string of a higher index. 0<-1, 1<-2 etc. Once that is done, we manually set our chamber's highest index to empty as values are copied, not moved, and having a bullet in
        //chamber[5] would result in our chamber suddenly having two Bullet strings. This is of course not the only way of moving strings like this.
        for (int i = 1; i < chamber.Length; i++)
        {
            chamber[i - 1] = chamber[i];
        }
        chamber[chamber.Length - 1] = "Empty";
        return false;
    }

    //This method RETURNS an array of strings once it has completed its code segment. Any method not marked void always has the possibility to return a variable according to its type. void methods always return null (a type representing nothing).
    //Do note however that every type of method which returns a reference type has the option of returning null. Value-type methods (int, float etc.) are not typically able to return null however. We will practice more reference types in a future workshop.
    static string[] RandomizeBullets()
    {
        //First of all we create an array variable holding our six strings. While possible to interpret the method as simply randomizing our existing chamber variable, this method instead assigns a new array to it. In this case, the end result is the same.
        string[] newChamber = new string[6];
        //Using a for-loop, we assign the string "Empty" to each of the strings in the array.
        for (int i = 0; i < newChamber.Length; i++)
        {
            newChamber[i] = "Empty";
        }
        //Lastly, we pick a random number between 0 and 5 using the Random class' Next method and assign the string "Bullet" to one of the strings in the newChamber array.
        int slotNumber = new Random().Next(0, newChamber.Length);
        newChamber[slotNumber] = "Bullet";
        //And this is where the magic happens. Since our newChamber variable is local (It is declared for the first time within a method and not a class variable (field)) any RAM memory allocated to it will be freed up once this method exits and what we have
        //done here will pretty much vanish into oblivion. To prevent this from happening we must make use of the method's return variable during the method call. For example, simply "typing RandomizeBullets();" within the code will result in this method
        //doing its thing followed by no real consequences because everything within this method has no impact on the overall program. However, assigning the end result to a variable like for example "string[] whateverName = RandomizeBullets();" will
        //store the end result in that variable! It's just like explicitly assigning a value to a variable yourself, but with infinitely more possibilities! 
        return newChamber;
    }
}

//And that concludes our Russian Roulette Game - Harmless HighScore Edition. We will keep practicing methods and loops in addition to instances and reference variables for our next workshop!