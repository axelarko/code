﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//This program exemplifies the use of an outer and an inner loop to create strings representing a complete deck of cards by simply only having the components predefined. This is useful to cover all possible combinations through iterations.
class Program
{
    static string[] colors = { "Hearts", "Spades", "Diamonds", "Clubs"};
    static string[] values = {"Ace", "Two", "Three", "Four", "Five", "Six",
        "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King" };

    static string[] deckOfCards = new string[colors.Length * values.Length];

    static void Main() 
    {
        int counter = 0;
        foreach(string color in colors)
        {
            foreach(string value in values)
            {
                Console.WriteLine("{0} of {1}", value, color);
                deckOfCards[counter] = string.Format("{0} of {1}", value, color);
                counter++;
            }
        }

        foreach(string card in deckOfCards)
        {
            Console.WriteLine(card);
        }
        Console.ReadKey();
    }
}

