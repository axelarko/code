﻿using System;
using System.Diagnostics;

public static class MainProgram
{
    static SnakeGame currentGame;

    static void Main()
    {
      new SnakeGame();
    }

    public static void SetupGame(SnakeGame newGame)
    {
        currentGame = newGame;
    }

    public static SnakeGame GetGame()
    {
        return currentGame;
    }
}

public class SnakeGame
{
    private Stopwatch timer;
    private int gameSpeed;
    private Unit[,] gameField;
    private PlayerUnit player;
    private Tail[] playerTail;
    private PowerUp powerUp;
    private int playerScore = 0;
    private int speedCap = 5;
    private int maxSpeed;
    private bool gameIsRunning = false;

    public SnakeGame()
    {
        MainProgram.SetupGame(this);
        timer = new Stopwatch();
        Console.CursorVisible = false;
        SetupGameBoard(40);
        timer.Start();
        gameSpeed = 200;
        maxSpeed = gameSpeed / speedCap;
        StartGameLoop();
    }

    void SetupGameBoard(int size)
    {
        Console.SetWindowSize(size * 2, size);
        gameField = new Unit[size, size*2];
        for (int indexOne = 0; indexOne < gameField.GetLength(1); indexOne++)
        {
            for (int indexTwo = 0; indexTwo < gameField.GetLength(0); indexTwo++)
            {
                gameField[indexTwo, indexOne] = new Unit(new int[] { indexOne, indexTwo});
            }
        }
        SpawnPlayer(size, size / 2);
        SpawnPowerUp();
    }

    void UpdatePlayerScore()
    {
        playerScore++;
        if(playerScore > 5)
        {
            playerScore = 0;
            IncreaseSpeed();
        }
    }

    void IncreaseSpeed()
    {
        gameSpeed -= 100;
        if(gameSpeed < maxSpeed)
        {
            gameSpeed = maxSpeed;
        }
    }

    void SpawnPlayer(int x, int y)
    {
        player = new PlayerUnit(new int[] {x, y});
        playerTail = new Tail[gameField.Length-1];
    }

    void StartGameLoop()
    {
        gameIsRunning = true;
        while(gameIsRunning)
        {
            UpdateGame();
            GetInput();
        }
    }

    public void CheckForTail()
    {
        foreach(Tail tailPiece in playerTail)
        {
            if (tailPiece != null)
            {
                if (player.Coordinates[0] == tailPiece.Coordinates[0] && player.Coordinates[1] == tailPiece.Coordinates[1])
                {
                    GameOver();
                }
            }
        }
    }

    public void GameOver()
    {
        gameIsRunning = false;
    }

    void GetInput()
    {
        if(Console.KeyAvailable == false)
        {
            return;
        }
        ConsoleKey playerInput = Console.ReadKey(true).Key;
        switch(playerInput)
        {
            case ConsoleKey.W:
            case ConsoleKey.UpArrow:
            case ConsoleKey.S:
            case ConsoleKey.DownArrow:
            case ConsoleKey.A:
            case ConsoleKey.LeftArrow:
            case ConsoleKey.D:
            case ConsoleKey.RightArrow:
                player.ChangeDirection(playerInput);
                break;
        }

    }

    void UpdateGraphics()
    {
        Console.SetCursorPosition(player.Coordinates[0], player.Coordinates[1]);
        Console.Write(0);
    }

    public void UpdatePlayerPosition(int[] oldPosition, int[] newPosition)
    {
        Console.SetCursorPosition(oldPosition[0], oldPosition[1]);
        Console.Write(" ");
        Console.SetCursorPosition(newPosition[0], newPosition[1]);
        Console.Write("O");

        for (int i = 0; i < playerTail.Length; i++)
        {
            if(playerTail[i] == null)
            {
                break;
            }
            Console.SetCursorPosition(playerTail[i].Coordinates[0], playerTail[i].Coordinates[1]);
            Console.Write("O");
        }
    }

    void UpdateGame()
    {
        if(timer.ElapsedMilliseconds >= gameSpeed)
        {
            MoveTail();
            player.Move();
            CheckForTail();
            timer.Restart();
            CheckForPowerUp();
        }
    }

    void MoveTail()
    {
        for(int i = playerTail.Length - 1; i >= 0; i--)
        { 
            if(playerTail[i] != null)
            {
                if(playerTail[i].IsAwake())
                {
                    if(i != 0)
                    {
                        playerTail[i].Move(playerTail[i - 1].Coordinates);
                    }
                    else
                    {
                        playerTail[i].Move(player.Coordinates);
                    }
                }
            }
        }
    }

    void CheckForPowerUp()
    {
        if(player.Coordinates[0] == powerUp.Coordinates[0] && player.Coordinates[1] == powerUp.Coordinates[1])
        {
            UpdatePlayerScore();
            ExtendPlayerTail();
            SpawnPowerUp();
        }

    }

    void SpawnPowerUp()
    {
        Random randomNumber = new Random();
        bool positionIsInvalid = true;
        int[] coordinates = new int[2];

        while (positionIsInvalid == true)
        {
            positionIsInvalid = false;
            coordinates = new int[] { randomNumber.Next(0, gameField.GetLength(1)), randomNumber.Next(0, gameField.GetLength(0)) };

            foreach (Tail tailPiece in playerTail)
            {
                if(tailPiece != null)
                if (tailPiece.Coordinates[0] == coordinates[0] && tailPiece.Coordinates[1] == coordinates[1])
                {
                    positionIsInvalid = true;
                }
            }
            if(player.Coordinates[0] == coordinates[0] && player.Coordinates[1] == coordinates[1])
            {
                positionIsInvalid = true;
            }
        }     
        powerUp = new PowerUp(coordinates);
        DrawPowerUp();
    }

    void DrawPowerUp()
    {
        Console.SetCursorPosition(powerUp.Coordinates[0], powerUp.Coordinates[1]);
        Console.Write("P");
    }

    void ExtendPlayerTail()
    {
        int currentTailLength = 0;
        for(int i = 0; i < playerTail.Length; i++)
        {
            if(playerTail[i] == null)
            {
                if (currentTailLength == 0)
                {
                    playerTail[i] = new Tail(player.Coordinates);
                }
                else
                {
                    playerTail[i] = new Tail(playerTail[i - 1].Coordinates);
                }
                return;
            }
            else
            currentTailLength++;
        }
    }

    public Unit GetUnit(int[] wantedCoordinates)
    {
        foreach (Unit unit in gameField)
        {
            if(unit.Coordinates[0] == wantedCoordinates[0] && unit.Coordinates[1] == wantedCoordinates[1])
            {
                return unit;
            }
        }
        return null;
    }
}

public class Unit
{
    protected int[] coordinates;
    public int[] Coordinates
    {
        get { return coordinates; }
    }

    public Unit(int[] coordinates)
    {
        this.coordinates = coordinates;
    }
}

public class PowerUp : Unit
{
    public PowerUp(int[] coordinates) : base(coordinates)
    {
        this.coordinates = coordinates;
    }

}

public class Tail : Unit
{
    private bool sleeping = true;
    public Tail(int[] coordinates) : base(coordinates)
    {
        this.coordinates = coordinates;
    }

    public void Move(int[] newCoordinates)
    {
        Unit nextUnit = MainProgram.GetGame().GetUnit(newCoordinates);
        if (nextUnit != null)
        {
            Console.SetCursorPosition(coordinates[0], coordinates[1]);
            Console.Write(" ");
            coordinates = nextUnit.Coordinates;
        }
    }

    public bool IsAwake()
    {
        if(sleeping)
        {
            sleeping = false;
            return false;
        }
        else
        {
            return true;
        }
    }
}


public class PlayerUnit : Unit
{
    enum Direction {Up, Down, Left, Right};
    Direction currentDirection;
    Direction lastMoved;
        
    public PlayerUnit(int[] coordinates) : base(coordinates)
    {
        this.coordinates = coordinates;
        currentDirection = Direction.Up;
        lastMoved = currentDirection;
    }

    public void ChangeDirection(ConsoleKey key)
    {
        switch(key)
        {
            case ConsoleKey.D:
            case ConsoleKey.RightArrow:
                if(lastMoved != Direction.Left)
                currentDirection = Direction.Right;
                break;

            case ConsoleKey.A:
            case ConsoleKey.LeftArrow:
                if (lastMoved != Direction.Right)
                currentDirection = Direction.Left;
                break;

            case ConsoleKey.W:
            case ConsoleKey.UpArrow:
                if (lastMoved != Direction.Down)
                currentDirection = Direction.Up;
                break;

            case ConsoleKey.S:
            case ConsoleKey.DownArrow:
                if (lastMoved != Direction.Up)
                currentDirection = Direction.Down;
                break;
        }
    }


    public void Move()
    {
        Unit nextUnit = null;
        int[] oldLocation = new int[] { coordinates[0], coordinates[1]};
        switch (currentDirection)
        {
            case Direction.Up:
                nextUnit = MainProgram.GetGame().GetUnit(new int[]{coordinates[0], coordinates[1]-1});
                break;

            case Direction.Left:
                nextUnit = MainProgram.GetGame().GetUnit(new int[] { coordinates[0] - 1, coordinates[1]});
                break;

            case Direction.Right:
                nextUnit = MainProgram.GetGame().GetUnit(new int[] { coordinates[0]+1, coordinates[1]});
                break;

            case Direction.Down:
                nextUnit = MainProgram.GetGame().GetUnit(new int[] { coordinates[0], coordinates[1]+1});
                break;

        }
        if (nextUnit != null)
        {
            coordinates = nextUnit.Coordinates;
            MainProgram.GetGame().UpdatePlayerPosition(oldLocation, coordinates);
            lastMoved = currentDirection;
        }
        else
        {
            MainProgram.GetGame().GameOver();
        }
    }
}