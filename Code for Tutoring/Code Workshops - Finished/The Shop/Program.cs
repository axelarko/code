﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


//In this program, we create a simple shopkeeper program which primarily practices the handling of strings through formatting them as well as parsing and comparing them with other strings. We also practice using and reusing stored variables
class Program
{
    static void Main(string[] args)
    {
        //For our shop, we need to start by designating our base variables. Remember, the first time a variable is mentioned to the program - when it is 'declared' - we need to follow a set pattern. (<type> <variableName> = <value>;) e.g. int currentYear = 2017; 
        //We cannot use spaces in variable names, so we use camelCase instead. Start with a lowercase letter and each time a space should occur, follow it up with an uppercase letter for the next word.

        //These values represent the player's money, the amount of items they want to buy as well as the total price of all items to purchase. We declare them here for later use.
        int playerMoney = 100;
        int amount = 0;
        int totalPrice = 0;

        //These variables represent our first item's name and price per piece.
        string itemOneName = "Apples";
        int itemOnePrice = 2;

        //Like above, but more expensive - because bananas.
        string itemTwoName = "Bananas";
        int itemTwoPrice = 5;

        //Here we declare a string variable which we use as an argument for the Console.WriteLine method. Note that we use the variable name as reference within the parentheses.
        string greeting = "Welcome to the Item Shop!";
        Console.WriteLine(greeting);
        //string.Format is useful for creating dynamic strings based on the values of different variables. The {x} are placeholder slots within the string. When the string is created, it will use the variable references at the places where the
        //placeholders are located. For this reason, string.Format is very useful for having one way of writing out different strings depending on the values of the variables.
        string currentPrices = string.Format("{0} are {1} a piece. {2} are {3} a piece.",itemOneName, itemOnePrice, itemTwoName, itemTwoPrice);

        Console.WriteLine(currentPrices);

        //We could reuse our greeting string and assign a new value instead of declaring a new string, but for clarity's sake, we simply declare a new one as we cannot change the variable names.
        string shoppingPrompt = "\nWhat would you like to buy?"; //Backslash n (\n) is a special character which makes the computer interpret it as a new line. In this case, the computer will jump to a new line before typing "What would you like to buy?"
        Console.WriteLine(shoppingPrompt);

        //Since we do not want the game to keep going in a scenario where the player chooses an invalid item from the shop (The shop only sells Apples and Bananas), we create a conditional loop to keep the program from proceeding until the condition is evaluated as "false"
        //Loops are super-inportant for flow control since the computer will always follow a set protocol when reading the code, reading it from top to bottom.

        //Before we start the loop, we create a bool value. This is the condition which will determine how long the program stays in the loop. For readability's sake, we name it 'playerIsChoosing' since it will easily be readable as "While player is choosing" for the loop declaration.
        bool playerIsChoosing = true;
        //As long as playerIsChoosing is true when the loop starts or ends, the code within the curly braces '{}' following the loop will run from top to bottom.
        while(playerIsChoosing)
        {
            //At the start of the loop we immidiately set playerIsChoosing to false. This does not mean the loop will end, as the conditional bool only is evaluated at the loops start and at the end of each iteration.
            playerIsChoosing = false;
            //Through the Console.ReadLine method we obtain a string, which is what the player types in on one line. This string is assigned to the playerInputString variable for later use.
            string playerInputString = Console.ReadLine();

            //Here we use the value in our playerInputString variable and compare it to the first item in our shop. Since comparisons need to be exact to evaluate true, we turn all letters in each string to uppercase during this comparison.
            //Note that this does not mean we change the value of our playerInputString, since we never assign a new value to it. This means the variable still holds the exact string the player typed in.
            if (playerInputString.ToUpper() == itemOneName.ToUpper())
            {
                //We use a formatted string here again, with the placeholders being linked to itemOneName and itemOnePrice. Note that it is fine to use the same placeholder on multiple locations in the same string, like using itemOneName twice here.
                string pricePrompt = string.Format("{0} huh? {0} are {1} a piece. How many would you like?", itemOneName, itemOnePrice);
                Console.WriteLine(pricePrompt);

                //Here we create yet another loop to keep the game from progressing unless the program is able to parse the inputString variable to an int. The numberIsInvalid will only be set to false if the TryParse method can successfully convert the
                //playerInputString variable to an int. (There is of course nothing currently stopping us from buying NEGATIVE amounts of apples as we did not have time to fix that during the workshop time. We could implement a fix for that
                //later though without too much trouble.
                bool numberIsInvalid = true;
                while (numberIsInvalid)
                {
                    playerInputString = Console.ReadLine();

                    if (int.TryParse(playerInputString, out amount))
                    {
                        numberIsInvalid = false;
                    }
                }
                string confirmationString = string.Format("You wanted to buy {0} which will cost you {1}", amount, itemOnePrice * amount);
                Console.WriteLine(confirmationString);

                //We assign the itemprice times the amount we wish to buy to the totalPrice variable here for later use.
                totalPrice = itemOnePrice * amount;
            }

            //This block does the same as the block above, but handles our second item instead.
            else if (playerInputString.ToUpper() == itemTwoName.ToUpper())
            {
                string pricePrompt =
                string.Format("{0} huh? {0} are {1} a piece. How many would you like?",
                itemTwoName, itemTwoPrice);
                Console.WriteLine(pricePrompt);

                bool numberIsInvalid = true;

                while (numberIsInvalid)
                {
                    playerInputString = Console.ReadLine();

                    if (int.TryParse(playerInputString, out amount))
                    {
                        numberIsInvalid = false;
                    }
                }
                string confirmationString = string.Format("You wanted to buy {0} which will cost you {1}", amount, itemTwoPrice * amount);
                Console.WriteLine(confirmationString);

                totalPrice = itemTwoPrice * amount;
            }

            //This is where we end up if our playerInputString is not corresponding to our first or second itemname. Here we set playerIsChoosing to true, which means this loop will repeat as long as we reach this block. We also tell the player to
            //either select item one or item two.
            else
            {
                string repeatString = string.Format("Please choose {0} or {1}", itemOneName, itemTwoName);
                Console.WriteLine(repeatString);
                playerIsChoosing = true;
            }
        }

        //The code reaches this part once the playerIsChoosing loop ends, which means we will have a value assigned to the totalPrice variable. Now all we need to do is check the price for the items we wish to purchase

        //I've added this part myself as we did not do it for our workshop. We had no check to see if the player tried to buy negative amounts of fruit, so this part catches that scenario and prevents it from happening.
        if(totalPrice < 0)
        {
            string cannotBuyPrompt = string.Format("Wait just a minute here! Trying to buy negative items are we? No deal!");
            Console.WriteLine(cannotBuyPrompt);
        }
        //If we actually bought a valid number, we need to check if we have the money for it. If we don't, this code tells the player to get richer or something.
        else if(totalPrice > playerMoney)
        {
            string cannotBuyPrompt = string.Format("Unfortunately, you don't have enough money. You need {0} more.", totalPrice - playerMoney);
            Console.WriteLine(cannotBuyPrompt);
        }

        //Lastly, this part is called if we actually bought a valid number of fruit we can afford. We deduct the price from playerMoney and tell them the purchase was successful!
        else
        {
            playerMoney -= totalPrice;
            string canBuyPrompt = string.Format("Thank you for your patronage. You have {0} money left.", playerMoney);
            Console.WriteLine(canBuyPrompt);
        }

        //This is just to prevent the code from exiting right away after our purchase prompt has been displayed.
        Console.ReadKey();
    }
}
