﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Properties : MonoBehaviour
{
    //Properties can be seen as special Fields (class variables) which can act similarly to Methods. They have the added benefit of being able to use logic within their operations, which can lead to safer and more efficient code.

    //This is a regular Field holding a string. For naming conventions, fields typically always start with a lowercase letter, and any spaces are signified by the first letter of the next word being capitalized.
    //This is typically called Camel Case. 
    private string exampleName;

    //This is a Property. Properties require a field to expose and work on, since they are not technically fields themselves. Properties' names are conventially written like Methods, with all first letters being
    //capitalized. This is typically called Pascal Case.

    //This Property encapsulates the exampleName field and is accessible just like the field variable itself. (e.g. ExampleName = "Anne Onymous"). The difference in this particular case is that the 'getter'
    //(The read part) of the variable is public, meaning other classes and objects can look at the string, but the 'setter' (The write part) is marked as private, meaning only the object itself can change its name.
    //What this essentially means is that we through the use of Properties can make private fields accessible but not modifiable from outside. Not only does this make for cleaner code, it also makes it more secure
    //and easy to debug.
    public string ExampleName
    {
        get { return exampleName; }
        private set { exampleName = value; }
    }

    //Properties are handy for making sure the field variable falls within reasonable values. In this example, both the getter and setter are public, meaning any object from outside can modify our life value.
    //However, the setter has some logic baked into it. No matter what we set our Life to, it will never fall below 0 (e.g. Life = -100; will still set life to 0).
    //We can thus prevent unintended behaviours as our variables are accessed.
    //The getter can also have operations placed into it, but be mindful so that those never actually modify the value in any way. That's what the setter is for.
    private int life;
    public int Life
    {
        get { return life; }
        set
        {
            life = value;
            if(life < 0)
            {
                life = 0;
            }
        }
    }

    //This is called an 'Auto Property'. They are more compact to write, but act exactly like a regular property. If one uses an Auto Property, 
    //the compiler will create a backing field (in this example it would be likened to 'private int attackPower' once the code compiles, so technically, this is no different from manually declaring the 'backing field'
    //as it is called. To be precise, this operates exactly like the ExampleName property.
    //Auto Properties are typically used when you don't need any logic within the getter or setter. Their main purpose is being a shorthand writing to setting up Properties.
    public int AttackPower
    {  get; private set;  }


    //Be mindful that while they can contain logic, a Property should not have too much complexity within it (You don't want to call any 'Find' operation within a Property for example). 
    //Save heavier operations for Methods and have those methods use the Properties just like they would use Field data.
    //What 'heavier operations' entail is not always obvious, but try not to have multiple loops nested in a Property. They should be fast.
}
