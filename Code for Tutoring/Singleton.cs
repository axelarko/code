﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton : MonoBehaviour
{
    //The Singleton Pattern uses a public static reference to one particular object. Doing this, the object essentially becomes globally accessible once assigned. This is useful for e.g. manager classes and 
    //dataholders multiple objects require access to. Note that the Singleton Pattern comes with certain limitations.

    //1 - The object itself must be non-static. This goes naturally with how Monobehaviour operates within Unity, so it is pretty much a non-issue.
    //2 - "There can be only one." The most important thing to note is that you may only have ONE(1) Singleton assigned per type. This means you should NEVER use the Singleton Pattern for classes which have multiple
    //instances active simultaneously. It is completely fine to reassign the Singleton reference in case you wish to replace the old one.
    //3 - Should the Singleton instance be destroyed in any way (e.g. through scene change or manually), the Singleton pointer (instance in this case) will give a nullpointer exception. When using the Singleton pattern,
    //make sure the reference object is persistent throughout the entire program's lifetime.

   
    //Setting up a Singleton is easy. Simply declare a public static variable with the SAME TYPE as the object you're creating a Singleton for. Typically, the variable name used for this pattern is "instance".
    public static Singleton instance;

    //For Unity's Monobehaviours, we can then simply assign the Singleton variable to the object through the Awake() method (Awake is called earlier than Start). Again, make sure objects implementing the Singleton
    //pattern are objects which there are no more than ONE of in the entire program or you will run into issues.
    private void Awake()
    {
        instance = this;
    }

    //Now that we have assigned the instance variable to our object, every single script we have can access this object and all public methods through '<Type>.instance'. We don't ever need to use any "Find" methods
    //to locate the object!

    //For example, this method is accessible through 'Singleton.instance.TestCall();' since Singleton is the name of the Class' Type
    public void TestCall()
    {
        Debug.Log(string.Format("{0} successfully called", this.GetType().Name));
    }

    //Singletons are easy to setup and use, as long as one is aware of their limitations and drawbacks.
}
