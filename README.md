# Axel Ark� C# Code Portfolio

This directory contains code that I have personally written for various projects. The code is designed for projects in UnityEngine, although parts of it are self-contained and can be modified to work elsewhere.
The intent of this directory is primarily to act as my personal portolio - to show my personal progress and how I work when I design my code.

Most of my stuff is aimed towards developer tools, special effects and data handling for RPG games. Though older code comes from my earlier projects and game jams.

The code contained here belongs to me and using it without contacting or crediting me is not permitted.

For personal contact, 
Email: axel.arko@gmail.com
LinkedIn: https://se.linkedin.com/in/axel-ark%C3%B6-ab191b113

